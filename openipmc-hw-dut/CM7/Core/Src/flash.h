#ifndef FLASH_H
#define FLASH_H




void flash_device_reset(void);
uint8_t flash_get_status_reg(int reg);
void flash_set_status_reg(int reg, uint8_t value);
_Bool flash_is_busy(void);
void flash_write_enable(void);

void flash_data_load( uint16_t column, uint32_t len, uint8_t* data );
void flash_quad_data_load( uint16_t column, uint32_t len, uint8_t* data );
void flash_program_execute( uint16_t page );

void flash_page_data_read( uint16_t page );
void flash_read( uint16_t column, uint32_t len, uint8_t* data);
void flash_fast_read_quad( uint16_t column, uint32_t len, uint8_t* data);

void flash_block_erase(uint16_t block);

uint8_t flash_qspi_test( void );




#endif
