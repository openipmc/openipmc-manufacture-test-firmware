
/*
 * Receives and executes commands from the TESTER device
 */

#include "main.h"

#include "FreeRTOS.h"
#include "stream_buffer.h"

#include "amc_gpios.h"
#include "head_commit_sha1.h"

// AMC driver initialization status and interrupt status to be reported to TESTER
extern uint8_t amc_init_status;
extern uint8_t amc_interrupt_test_flags[90];

// QSPI Flash tests
extern uint8_t flash_qspi_status;

// UART interface
extern UART_HandleTypeDef huart5;
#define huart_tester huart5
StreamBufferHandle_t tester_command_input_stream;


// Buffer for received command
static uint8_t cmd[8]; // 8 bytes command (fixed)
static int     cmd_index = 0;



// First command byte: START
#define CMD_START          0x00

// Second command byte: function code (from shared header)
#include "test_function_codes.h"

// The 5 command bytes in the middle are interpreted according to the "function" (2nd byte).

// Last command byte (8th): STOP
#define CMD_STOP           0xFF





// GPIO port map: organizes the port pointers in an array
static GPIO_TypeDef* gpio_port_map[11] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG, GPIOH, GPIOI, GPIOJ, GPIOK};


static void execute_command( void );



#define GPIO_CONFIGURE_PIN(PORT, NUMBER, MODE, PULL)   \
	{                                                  \
		GPIO_InitTypeDef GPIO_InitStruct = {0};        \
		GPIO_InitStruct.Pin  = NUMBER;                 \
		GPIO_InitStruct.Mode = MODE;                   \
		GPIO_InitStruct.Pull = PULL;                   \
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;   \
		HAL_GPIO_Init( PORT, &GPIO_InitStruct);        \
	}



void tester_command_processor_task(void *argument)
{
	char input_char;

	tester_command_input_stream = xStreamBufferCreate(32, 1);

	while(1)
	{
		xStreamBufferReceive( tester_command_input_stream, &input_char, 1, portMAX_DELAY );

		if( (cmd_index == 0) && (input_char == CMD_START) ) // First byte: check start
		{
			cmd[cmd_index++] = input_char;
		}
		else if( (cmd_index >= 1) && (cmd_index <= 6) ) // Middle bytes
		{
			cmd[cmd_index++] = input_char;
		}
		else if( (cmd_index == 7) && (input_char == CMD_STOP)) // Last byte: check stop and execute
		{
			cmd[cmd_index] = input_char;
			cmd_index = 0;
			execute_command();
		}
		else
			cmd_index = 0; // Reset. Only if some error in command alignment happens.
	}
}




static void execute_command( void )
{

	uint8_t ret[12] = {0}; // Default return to TESTER

	switch( cmd[1] ) // Check function byte
	{
		case CMD_GPIO_SET_PIN_HIGH:
			HAL_GPIO_WritePin( gpio_port_map[cmd[2]], ((uint16_t)cmd[4]<<8)+cmd[3], GPIO_PIN_SET );
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_GPIO_SET_PIN_LOW:
			HAL_GPIO_WritePin( gpio_port_map[cmd[2]], ((uint16_t)cmd[4]<<8)+cmd[3], GPIO_PIN_RESET );
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_GPIO_READ_PIN:
			if( HAL_GPIO_ReadPin(gpio_port_map[cmd[2]], ((uint16_t)cmd[4]<<8)+cmd[3] ) == GPIO_PIN_RESET)
				ret[0] = 0;
			else
				ret[0] = 1;
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_GET_MCU_UID:
			ret[0]  = (HAL_GetUIDw0() >> 0 ) & 0x000000FF;
			ret[1]  = (HAL_GetUIDw0() >> 8 ) & 0x000000FF;
			ret[2]  = (HAL_GetUIDw0() >> 16) & 0x000000FF;
			ret[3]  = (HAL_GetUIDw0() >> 24) & 0x000000FF;
			ret[4]  = (HAL_GetUIDw1() >> 0 ) & 0x000000FF;
			ret[5]  = (HAL_GetUIDw1() >> 8 ) & 0x000000FF;
			ret[6]  = (HAL_GetUIDw1() >> 16) & 0x000000FF;
			ret[7]  = (HAL_GetUIDw1() >> 24) & 0x000000FF;
			ret[8]  = (HAL_GetUIDw2() >> 0 ) & 0x000000FF;
			ret[9]  = (HAL_GetUIDw2() >> 8 ) & 0x000000FF;
			ret[10] = (HAL_GetUIDw2() >> 16) & 0x000000FF;
			ret[11] = (HAL_GetUIDw2() >> 24) & 0x000000FF;

			HAL_UART_Transmit(&huart_tester, ret, 12, 1000); // UID has 96 bits
			break;

		case CMD_AMC_SET_PIN_HIGH:
			amc_gpios_write_pin( cmd[2], 1 );
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_AMC_SET_PIN_LOW:
			amc_gpios_write_pin( cmd[2], 0 );
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_AMC_READ_PIN:
			if( amc_gpios_read_pin( cmd[2] ) == 0 )
				ret[0] = 0;
			else
				ret[0] = 1;
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_AMC_SET_PIN_INT:
			amc_gpios_set_pin_interrupt( cmd[2], cmd[3]  );
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_AMC_GET_STATUS:
			ret[0] = amc_init_status;
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_AMC_GET_INT_FLAGS:
			ret[0] = amc_interrupt_test_flags[cmd[2]];
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			amc_interrupt_test_flags[cmd[2]] = 0; // Clear the flags since they are already informed to TESTER
			break;

		case CMD_GET_VERSION:
			ret[0]  = (HEAD_COMMIT_SHA1 >> 0 ) & 0x000000FF;
			ret[1]  = (HEAD_COMMIT_SHA1 >> 8 ) & 0x000000FF;
			ret[2]  = (HEAD_COMMIT_SHA1 >> 16) & 0x000000FF;
			ret[3]  = (HEAD_COMMIT_SHA1 >> 24) & 0x000000FF;
			HAL_UART_Transmit(&huart_tester, ret, 4, 1000);
			break;

		case CMD_SET_SWD_TO_GPIO:
			GPIO_CONFIGURE_PIN( GPIOA, GPIO_PIN_14,  GPIO_MODE_INPUT, GPIO_PULLUP ); // Set TCK/SWCLK as GPIO input
			GPIO_CONFIGURE_PIN( GPIOA, GPIO_PIN_13,  GPIO_MODE_INPUT, GPIO_PULLUP ); // Set TMS/SWDIO as GPIO input
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;

		case CMD_QSPI_GET_STATUS:
			ret[0] = flash_qspi_status;
			HAL_UART_Transmit(&huart_tester, ret, 1, 1000);
			break;
	}
}
