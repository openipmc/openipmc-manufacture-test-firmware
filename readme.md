# Manufacture Test Firmware

Developed for testing the [OpenIPMC-HW](https://gitlab.com/openipmc/openipmc-hw)  boards after production. It checks solder integrity by checking the internal and external signals.

The test uses two miniDIMM cards, one behaving as *Tester* (a reliable board must be elected) and the *DUT* (Device Under Test). Both miniDIMM cards are connected together by using one [MTB](https://gitlab.com/openipmc/openipmc-hw_debug-base/-/tree/MTB) MTB test board. This repository contains two firmware projects developed on STM32CubeIDE 1.5.0, one for *Tester*, and other for *DUT*. 

Both firmware project are automatically compiled via CI/CD in the branch `master`, and both `.bin` files are available for download as pipeline artifacts. To avoid redundancy, new commits with no change in the firmware will not have any binary to be downloaded.

Click 

## Progamming (DFU)

miniDIMM cards can be programmed by using DFU. This process requires the cards to be connected to a PC via USB and a DFU tool installed on it (eg. `dfu-util` for Ubuntu).

When connected to a PC, the miniDIMM card is powered. The next step force the MCU boot the *built-in bootloader* present on its ROM:

1. Press and **hold** the button BOOT
2. Pulse the button RESET
3. Release button BOOT
4. Check if the device is listed by `dfu-util -l`.  The output should be as below:

```console 
Found DFU: [0483:df11] ver=0200, devnum=71, cfg=1, intf=0, path="5-1", alt=1, name="@Option Bytes   /0x5200201C/01*128 e", serial="200364500000"
Found DFU: [0483:df11] ver=0200, devnum=71, cfg=1, intf=0, path="5-1", alt=0, name="@Internal Flash   /0x08000000/16*128Kg", serial="200364500000"
```

The next step is program the device. The binary (either for *Tester* or *DUT*) must be written into the MCU Flash, using the address `0x08000000`. Example:

```console
$ dfu-util -a 0 --dfuse-address 0x08000000 -D openipmc-hw-XXX_CM7.bin
```

## LEDs

The proper operation of the *Tester* and *DUT* cards can be checked from the LEDs present on MTB, according to its blinking pattern, as following:

- **Tester:**  Makes the LED *BL* blink in 1Hz.
- **DUT:**  Makes the LEDs *L0*, *L1* and *L2* blink in sequence, with total period of 1 second (*L0* &rarr; *L1* &rarr; *L2* &rarr; *L0* &rarr; ...)

## Performing the test

*Tester* has a CLI (Command Line Interface) available via USB as a serial interface. It is listed as `/dev/ttyAMCn` and can be accessed via `minicom`:

```console
$ minicom -D /dev/ttyACM0
```

**NOTE:** The Linux will probably try to control the device as a modem due to the current implementation of the *USB Device Class*. As a result the system will try to send AT command to the miniDIMM card. To prevent so, please disable the `ModemManager` service:

```console
# systemctl stop ModemManager
```

With both miniDIMM cards installed on the MTB, programmed and running (check the LEDs), type `test` on the CLI to run the test.
