all:
	@cd openipmc-hw-tester/CM7 && make all && cd -
	@cp -p openipmc-hw-tester/CM7/openipmc-hw-tester_CM7.bin ./
	
	@cd openipmc-hw-dut/CM7 && make all && cd -
	@cp -p openipmc-hw-dut/CM7/openipmc-hw-dut_CM7.bin ./
	
clean:
	@cd openipmc-hw-tester/CM7 && make clean && cd -
	@rm -f openipmc-hw-tester_CM7.bin
	
	@cd openipmc-hw-dut/CM7 && make clean && cd -
	@rm -f openipmc-hw-dut_CM7.bin
	
