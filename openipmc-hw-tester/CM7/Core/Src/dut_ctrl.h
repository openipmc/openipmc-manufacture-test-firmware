#ifndef DUT_CTRL_H
#define DUT_CTRL_H

#include <stdbool.h>
#include "amc_gpios.h"


void    dut_ctrl_gpio_write_pin( GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState );
uint8_t dut_ctrl_gpio_read_pin ( GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

bool dut_ctrl_get_mcu_uid( uint8_t uid[12] );

void    dut_ctrl_amc_write_pin( uint8_t amc_pin, uint8_t pin_value );
void    dut_ctrl_amc_set_pin_interrupt( uint8_t amc_pin, amc_int_mode_t mode );
uint8_t dut_ctrl_amc_read_pin(  uint8_t amc_pin );
uint8_t dut_ctrl_amc_get_status( void );

amc_int_status_t dut_ctrl_amc_get_int_flag( uint8_t amc_pin );

uint32_t dut_ctrl_get_version( void );
void     dut_set_swd_to_gpio( void );

uint8_t dut_ctrl_qspi_get_status( void );

#endif
