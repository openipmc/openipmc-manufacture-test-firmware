

/*
 * API for controlling the DUT
 *
 * It generates commands which are transmitted via UART to the DUT (Device Under Test)
 *
 * All functions from this API return under a response from DUT (at least 1 byte)
 */

#include <stdbool.h>
#include "main.h"

#include "FreeRTOS.h"
#include "stream_buffer.h"

#include "amc_gpios.h"

// UART interface
extern UART_HandleTypeDef huart5;
#define huart_dut huart5
extern StreamBufferHandle_t dut_ctrl_input_stream;



// First command byte: START
#define CMD_START          0x00

// Second command byte: function code (from shared header)
#include "test_function_codes.h"

// The 5 command bytes in the middle are interpreted according to the "function" (2nd byte).

// Last command byte (8th): STOP
#define CMD_STOP           0xFF






// GPIO port map: organizes the port pointers in an array
static GPIO_TypeDef* gpio_port_map[11] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG, GPIOH, GPIOI, GPIOJ, GPIOK};

// Return the index of gpio_port_map given GPIOx
static uint8_t port_ptr_to_num( GPIO_TypeDef *GPIOx );

static TickType_t dut_response_timeout = pdMS_TO_TICKS(1000);
//static TickType_t dut_response_timeout = portMAX_DELAY;  // Useful for debug
char no_response_from_dut[] = "NO RESPONSE FROM DUT\r\n";


// Write GPIO pin in the DUT
void dut_ctrl_gpio_write_pin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
	uint8_t cmd[8] = {CMD_START, 0, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	if(PinState == GPIO_PIN_RESET)
		cmd[1] = CMD_GPIO_SET_PIN_LOW;
	else
		cmd[1] = CMD_GPIO_SET_PIN_HIGH;

	cmd[2] = port_ptr_to_num( GPIOx ); //Port number according to port map

	cmd[3] = (uint8_t)((GPIO_Pin>>0)&0x00FF); // LSB first
	cmd[4] = (uint8_t)((GPIO_Pin>>8)&0x00FF);

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);
}


// Read GPIO pin in the DUT
uint8_t dut_ctrl_gpio_read_pin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	uint8_t cmd[8] = {CMD_START, CMD_GPIO_READ_PIN, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	cmd[2] = port_ptr_to_num( GPIOx ); //Port number according to port map

	cmd[3] = (uint8_t)((GPIO_Pin>>0)&0x00FF); // LSB first
	cmd[4] = (uint8_t)((GPIO_Pin>>8)&0x00FF);

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);

	return ret; // 0 if LOW; 1 if HIGH
}


// Get the MCU unique ID (little endian, LSB first, 12 bytes)
bool dut_ctrl_get_mcu_uid( uint8_t uid[12] )
{
	uint8_t cmd[8] = {CMD_START, CMD_GET_MCU_UID, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;
	bool    resp_status = true;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);

	for( int i=0; i<12; i++) // This command expects 12 bytes from DUT
	{
		if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		{
			resp_status = false;
			break;
		}
		uid[i] = ret;
	}

	return resp_status; // Return false if DUT is not responsive
}


// Write AMC pin in the DUT
void dut_ctrl_amc_write_pin( uint8_t amc_pin, uint8_t pin_value )
{
	uint8_t cmd[8] = {CMD_START, 0, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	if(pin_value == 0)
		cmd[1] = CMD_AMC_SET_PIN_LOW;
	else
		cmd[1] = CMD_AMC_SET_PIN_HIGH;

	cmd[2] = amc_pin;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);
}



// Set AMC pin interrupt enable
void dut_ctrl_amc_set_pin_interrupt( uint8_t amc_pin, amc_int_mode_t mode )
{
	uint8_t cmd[8] = {CMD_START, CMD_AMC_SET_PIN_INT, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	cmd[2] = amc_pin;
	cmd[3] = mode;

	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);
}

// Read AMC pin in the DUT
uint8_t dut_ctrl_amc_read_pin( uint8_t amc_pin )
{
	uint8_t cmd[8] = {CMD_START, CMD_AMC_READ_PIN, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	cmd[2] = amc_pin;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);

	return ret; // 0 if LOW; 1 if HIGH
}

// Read AMC initialization status in the DUT
uint8_t dut_ctrl_amc_get_status( void )
{
	uint8_t cmd[8] = {CMD_START, CMD_AMC_GET_STATUS, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);

	return ret;
}

// Get the AMC interrupt flags
amc_int_status_t dut_ctrl_amc_get_int_flag( uint8_t amc_pin )
{
	uint8_t cmd[8] = {CMD_START, CMD_AMC_GET_INT_FLAGS, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	cmd[2] = amc_pin;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);

	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);

	return (amc_int_status_t)ret;
}

// Get the version of the DUT firmware
uint32_t dut_ctrl_get_version( void )
{
	uint8_t  cmd[8] = {CMD_START, CMD_GET_VERSION, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t  ret;
	uint32_t version = 0;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);

	for( int i=0; i<4; i++) // This command expects 4 bytes from DUT
	{
		if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		{
			mt_printf("%s", no_response_from_dut);
			break;
		}
		version += ret << (i*8);
	}

		return version;
}



// Change the SWD pins on DUT into GPIOs
void dut_set_swd_to_gpio( void )
{
	uint8_t  cmd[8] = {CMD_START, CMD_SET_SWD_TO_GPIO, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t  ret;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);
}



// Read QSPI status in the DUT
uint8_t dut_ctrl_qspi_get_status( void )
{
	uint8_t cmd[8] = {CMD_START, CMD_QSPI_GET_STATUS, 0, 0, 0, 0, 0, CMD_STOP};
	uint8_t ret;

	xStreamBufferReset( dut_ctrl_input_stream );
	HAL_UART_Transmit(&huart_dut, cmd, 8, 1000);
	if( xStreamBufferReceive( dut_ctrl_input_stream, &ret, 1, dut_response_timeout ) == 0 )
		mt_printf("%s", no_response_from_dut);

	return ret;
}


static uint8_t port_ptr_to_num( GPIO_TypeDef *GPIOx )
{
	for( int i=0; i<11; ++i )
		if(GPIOx == gpio_port_map[i])
			return (uint8_t)i;
}
