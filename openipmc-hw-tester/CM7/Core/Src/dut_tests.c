
#include <stdbool.h>
#include <string.h>
#include "main.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "lwip/udp.h"

#include "head_commit_sha1.h"
#include "dut_ctrl.h"
#include "amc_gpios.h"

int mt_printf(const char* format, ...);


// Resource for Network test
static QueueHandle_t udp_recv_queue;

// Resource for Hardware Address test
GPIO_TypeDef* ha_ports[8] = { HW_0_GPIO_Port, HW_1_GPIO_Port, HW_2_GPIO_Port, HW_3_GPIO_Port,
                              HW_4_GPIO_Port, HW_5_GPIO_Port, HW_6_GPIO_Port, HW_7_GPIO_Port };
uint16_t      ha_pins[8]  = { HW_0_Pin, HW_1_Pin, HW_2_Pin, HW_3_Pin,
                              HW_4_Pin, HW_5_Pin, HW_6_Pin, HW_7_Pin };

// Resource for IPM test
GPIO_TypeDef* ipm_ports[16] = { IPM_0_GPIO_Port,  IPM_1_GPIO_Port,  IPM_2_GPIO_Port,  IPM_3_GPIO_Port,
                                IPM_4_GPIO_Port,  IPM_5_GPIO_Port,  IPM_6_GPIO_Port,  IPM_7_GPIO_Port,
                                IPM_8_GPIO_Port,  IPM_9_GPIO_Port,  IPM_10_GPIO_Port, IPM_11_GPIO_Port,
                                IPM_12_GPIO_Port, IPM_13_GPIO_Port, IPM_14_GPIO_Port, IPM_15_GPIO_Port };
uint16_t      ipm_pins[16]  = { IPM_0_Pin,  IPM_1_Pin,  IPM_2_Pin,  IPM_3_Pin,
                                IPM_4_Pin,  IPM_5_Pin,  IPM_6_Pin,  IPM_7_Pin,
                                IPM_8_Pin,  IPM_9_Pin,  IPM_10_Pin, IPM_11_Pin,
                                IPM_12_Pin, IPM_13_Pin, IPM_14_Pin, IPM_15_Pin };
// Resources for USR_IO test
GPIO_TypeDef* usr_io_ports[35] = { USR_IO_0_GPIO_Port,  USR_IO_1_GPIO_Port,  USR_IO_2_GPIO_Port,  USR_IO_3_GPIO_Port,
                                   USR_IO_4_GPIO_Port,  USR_IO_5_GPIO_Port,  USR_IO_6_GPIO_Port,  USR_IO_7_GPIO_Port,
                                   USR_IO_8_GPIO_Port,  USR_IO_9_GPIO_Port,  USR_IO_10_GPIO_Port, USR_IO_11_GPIO_Port,
                                   USR_IO_12_GPIO_Port, USR_IO_13_GPIO_Port, USR_IO_14_GPIO_Port, USR_IO_15_GPIO_Port,

                                   USR_IO_16_GPIO_Port, USR_IO_17_GPIO_Port, USR_IO_18_GPIO_Port, USR_IO_19_GPIO_Port,
                                   USR_IO_20_GPIO_Port, USR_IO_21_GPIO_Port, USR_IO_22_GPIO_Port, USR_IO_23_GPIO_Port,
                                   USR_IO_24_GPIO_Port, USR_IO_25_GPIO_Port, USR_IO_26_GPIO_Port, USR_IO_27_GPIO_Port,
                                   USR_IO_28_GPIO_Port, USR_IO_29_GPIO_Port, USR_IO_30_GPIO_Port, USR_IO_31_GPIO_Port,
                                   USR_IO_32_GPIO_Port, USR_IO_33_GPIO_Port, USR_IO_34_GPIO_Port                       };
uint16_t      usr_io_pins[35]  = { USR_IO_0_Pin,  USR_IO_1_Pin,  USR_IO_2_Pin,  USR_IO_3_Pin,
                                   USR_IO_4_Pin,  USR_IO_5_Pin,  USR_IO_6_Pin,  USR_IO_7_Pin,
                                   USR_IO_8_Pin,  USR_IO_9_Pin,  USR_IO_10_Pin, USR_IO_11_Pin,
                                   USR_IO_12_Pin, USR_IO_13_Pin, USR_IO_14_Pin, USR_IO_15_Pin,
                                   USR_IO_16_Pin, USR_IO_17_Pin, USR_IO_18_Pin, USR_IO_19_Pin,
                                   USR_IO_20_Pin, USR_IO_21_Pin, USR_IO_22_Pin, USR_IO_23_Pin,
                                   USR_IO_24_Pin, USR_IO_25_Pin, USR_IO_26_Pin, USR_IO_27_Pin,
                                   USR_IO_28_Pin, USR_IO_29_Pin, USR_IO_30_Pin, USR_IO_31_Pin,
                                   USR_IO_32_Pin, USR_IO_33_Pin, USR_IO_34_Pin                 };

// Resources for IPMB-A (index 0) and IPMB-B (index 1) tests
GPIO_TypeDef* ipmb_en_port[2]  = { IPMB_A_EN_GPIO_Port,  IPMB_B_EN_GPIO_Port  };
uint16_t      ipmb_en_pin[2]   = { IPMB_A_EN_Pin,        IPMB_B_EN_Pin        };
GPIO_TypeDef* ipmb_rdy_port[2] = { IPMB_A_RDY_GPIO_Port, IPMB_B_RDY_GPIO_Port };
uint16_t      ipmb_rdy_pin[2]  = { IPMB_A_RDY_Pin,       IPMB_B_RDY_Pin       };
GPIO_TypeDef* ipmb_scl_port[2] = { IPMB_A_SCL_GPIO_Port, IPMB_B_SCL_GPIO_Port };
uint16_t      ipmb_scl_pin[2]  = { IPMB_A_SCL_Pin,       IPMB_B_SCL_Pin       };
GPIO_TypeDef* ipmb_sda_port[2] = { IPMB_A_SDA_GPIO_Port, IPMB_B_SDA_GPIO_Port };
uint16_t      ipmb_sda_pin[2]  = { IPMB_A_SDA_Pin,       IPMB_B_SDA_Pin       };

// Resources for LEDs 0, 1 and 3 tests
GPIO_TypeDef* led_port[3]  = { FP_LED_0_GPIO_Port, FP_LED_1_GPIO_Port, FP_LED_2_GPIO_Port  };
uint16_t      led_pin[3]   = { FP_LED_0_Pin,       FP_LED_1_Pin,       FP_LED_2_Pin        };


#define TESTER_GPIO_CONFIGURE_PIN(PORT, NUMBER, MODE, PULL)   \
	{                                                         \
		GPIO_InitTypeDef GPIO_InitStruct = {0};               \
		GPIO_InitStruct.Pin  = NUMBER;                        \
		GPIO_InitStruct.Mode = MODE;                          \
		GPIO_InitStruct.Pull = PULL;                          \
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;          \
		HAL_GPIO_Init( PORT, &GPIO_InitStruct);               \
	}


// Test Functions
static bool dut_test_ntwork(void);
static void dut_test_gpio_input_print_single( GPIO_TypeDef* GPIOx,   uint16_t GPIO_Pin,   char* test_label );
static void dut_test_gpio_input_print_group ( GPIO_TypeDef* GPIOx[], uint16_t GPIO_Pin[], int num_of_pins, char* test_label, char* pin_label );
static uint8_t dut_test_amc_status_print( void );
static void dut_test_12v_en_print( void );
static void dut_test_amc_input_print_group( void );
static void dut_test_amc_interrupt_print( void );
static void dut_test_ipmb_ab_print( uint8_t channel );
static void dut_test_i2c_input_print( GPIO_TypeDef* GPIOx_SCL, uint16_t GPIO_Pin_SCL, GPIO_TypeDef* GPIOx_SDA, uint16_t GPIO_Pin_SDA, char* test_label );
static void dut_test_blue_led_print( void );
static void dut_test_leds_print( int led_num );
static void dut_test_uart0_input_print( void );
static void dut_test_qspi_flash_print( void );



/*
 * Executes the tests over DUT and prints the report
 */
void dut_test_sequence( void )
{
	bool dut_is_responsive;
	bool dut_has_same_version;


	// Header: Tester details
	mt_printf("\r\n\r\n\r\n");
	for(int i=0; i<80; i++) mt_printf("*");
	mt_printf("\r\n\r\n");
	mt_printf("OpenIPMC-HW Manufacturing Test");
	mt_printf("\r\n\r\n");
	mt_printf("Tester commit hash: %08X", HEAD_COMMIT_SHA1);
	mt_printf("\r\n");
	for(int i=0; i<80; i++) mt_printf("*");
	mt_printf("\r\n\r\n");


	//Check DUT responsivity and print MCU UID
	uint8_t uid[12];
	if( dut_ctrl_get_mcu_uid( uid ) )
	{
		dut_is_responsive = true;
		mt_printf("MCU UID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", uid[11], uid[10], uid[9], uid[8], uid[7], uid[6], uid[5], uid[4], uid[3], uid[2], uid[1], uid[0]);
	}
	else
	{
		dut_is_responsive = false;
		mt_printf("DEVICE IS NOT RESPONDING\r\n");
	}
	mt_printf("\r\n");

	// Check version of DUT firmware. It must be the same as this TESTER version
	uint32_t dut_version = dut_ctrl_get_version();
	if( dut_is_responsive )
	{
		if( dut_version == HEAD_COMMIT_SHA1 )
			dut_has_same_version = true;
		else
		{
			dut_has_same_version = false;
			mt_printf("DUT firmware is INCOMPATIBLE. Commit hash: %08X", dut_version);
		}
	}

	// Start Tests
	if( dut_is_responsive && dut_has_same_version )
	{
		// Ethernet Link
		mt_printf("Ethernet Link: ");
		if( dut_test_ntwork() )
			mt_printf("OK\r\n");
		else
			mt_printf("FAIL\r\n");

		// GPIOs
		dut_test_gpio_input_print_group( ha_ports,  ha_pins,   8, "Hardware Address Pins", "HA"     );
		dut_test_gpio_input_print_group( ipm_ports, ipm_pins, 16, "IPM Pins"             , "IPM"    );
		dut_test_gpio_input_print_group( usr_io_ports, usr_io_pins, 35, "USR IO Pins"    , "USR_IO" );
		if( dut_test_amc_status_print() == 0 )
		{
			dut_test_amc_input_print_group();
			dut_test_amc_interrupt_print();
		}
		mt_printf("\r\n");
		dut_test_12v_en_print();
		dut_test_gpio_input_print_single( HANDLE_GPIO_Port,       HANDLE_Pin ,     "Handle"        );
		dut_test_gpio_input_print_single( PYLD_RESET_GPIO_Port,   PYLD_RESET_Pin,  "PAYLOAD_RESET" );
		dut_test_gpio_input_print_single( ALARM_A_GPIO_Port,      ALARM_A_Pin,     "ALARM_A"       );
		dut_test_gpio_input_print_single( ALARM_B_GPIO_Port,      ALARM_B_Pin,     "ALARM_B"       );
		dut_test_gpio_input_print_single( PWR_GOOD_A_GPIO_Port,   PWR_GOOD_A_Pin,  "PWR_GOOD_A"    );
		dut_test_gpio_input_print_single( PWR_GOOD_B_GPIO_Port,   PWR_GOOD_B_Pin,  "PWR_GOOD_B"    );
		mt_printf("\r\n");
		dut_test_gpio_input_print_single( MASTER_TDI_GPIO_Port,   MASTER_TDI_Pin,  "MASTER_TDI"  );
		dut_test_gpio_input_print_single( MASTER_TDO_GPIO_Port,   MASTER_TDO_Pin,  "MASTER_TDO"  );
		dut_test_gpio_input_print_single( MASTER_TMS_GPIO_Port,   MASTER_TMS_Pin,  "MASTER_TMS"  );
		dut_test_gpio_input_print_single( MASTER_TCK_GPIO_Port,   MASTER_TCK_Pin,  "MASTER_TCK"  );
		dut_test_gpio_input_print_single( MASTER_TRST_GPIO_Port,  MASTER_TRST_Pin, "MASTER_TRST" );
		mt_printf("\r\n");
		dut_test_ipmb_ab_print(0); // IPMB-A
		dut_test_ipmb_ab_print(1); // IPMB-B
		dut_test_i2c_input_print( MGM_SCL_GPIO_Port,   MGM_SCL_Pin,     MGM_SDA_GPIO_Port,    MGM_SDA_Pin,    "MGM I2C" );
		dut_test_i2c_input_print( I2C3_SCL_GPIO_Port,  I2C3_SCL_Pin,    I2C3_SDA_GPIO_Port,   I2C3_SDA_Pin,   "IPMB-L/SENSE True I2C" );
		dut_test_i2c_input_print( I2C_BB_SCL_GPIO_Port, I2C_BB_SCL_Pin, I2C_BB_SDA_GPIO_Port, I2C_BB_SDA_Pin, "IPMB-L/SENSE Soft I2C" );
		mt_printf("\r\n");
		dut_test_blue_led_print();
		dut_test_leds_print( 2 );
		dut_test_leds_print( 1 );
		dut_test_leds_print( 0 );
		mt_printf("\r\n");
		dut_test_uart0_input_print();
		mt_printf("\r\n");
		dut_test_gpio_input_print_single( MCU_TDI_GPIO_Port,   MCU_TDI_Pin,  "MCU_TDI"  );
		dut_test_gpio_input_print_single( MCU_TDO_GPIO_Port,   MCU_TDO_Pin,  "MCU_TDO"  );
		TESTER_GPIO_CONFIGURE_PIN( GPIOA, GPIO_PIN_14,  GPIO_MODE_INPUT, GPIO_PULLUP ); // Set TCK/SWCLK as GPIO input
		TESTER_GPIO_CONFIGURE_PIN( GPIOA, GPIO_PIN_13,  GPIO_MODE_INPUT, GPIO_PULLUP ); // Set TMS/SWDIO as GPIO input
		dut_set_swd_to_gpio();
		dut_test_gpio_input_print_single( GPIOA,   GPIO_PIN_14,  "MCU_TCK/SWCLK"  );
		dut_test_gpio_input_print_single( GPIOA,   GPIO_PIN_13,  "MCU_TMS/SWDIO"  );
		mt_printf("\r\n");
		dut_test_qspi_flash_print();

	}
	mt_printf("\r\n");


	for(int i=0; i<80; i++) mt_printf("*");
}









/*
 * DUT GPIO input test
 *
 * In this test the the GPIO on DUT is kept as input, with internal pull-up.
 * TESTER checks:
 *     if the pin is HIGH when nothing is applied to the pin (TESTER also as input)
 *     if the pin is LOW when TESTER drives LOW to the pin
 *
 * Returns TRUE if test PASS.
 */
static bool dut_test_gpio_input( GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin )
{
	bool pin_is_ok = true;

	uint8_t pin_val = dut_ctrl_gpio_read_pin(GPIOx, GPIO_Pin);

	if( pin_val ) // Due to internal pull-up, pin must be high
	{
		// Tester drives LOW for checking contact
		TESTER_GPIO_CONFIGURE_PIN( GPIOx, GPIO_Pin, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL );
		HAL_GPIO_WritePin( GPIOx, GPIO_Pin, GPIO_PIN_RESET );

		// Check: LOW is expected
		pin_val = dut_ctrl_gpio_read_pin(GPIOx, GPIO_Pin);
		if( pin_val != 0)
			pin_is_ok = false;

		// Tester goes back to High Z
		TESTER_GPIO_CONFIGURE_PIN( GPIOx, GPIO_Pin, GPIO_MODE_INPUT, GPIO_PULLUP );
	}
	else
		pin_is_ok = false;

	return pin_is_ok;
}



/*
 * DUT AMC input test
 *
 * In this test the the AMC pin on DUT is kept as input, with internal pull-up.
 * TESTER checks:
 *     if the pin is HIGH when nothing is applied to the pin (TESTER also as input)
 *     if the pin is LOW when TESTER drives LOW to the pin
 *
 * Returns TRUE if test PASS.
 */
static bool dut_test_amc_input( uint8_t amc_pin )
{
	bool pin_is_ok = true;

	uint8_t pin_val = dut_ctrl_amc_read_pin( amc_pin );

	if( pin_val ) // Due to internal pull-up, pin must be high
	{
		// Tester drives LOW for checking contact
		amc_gpios_set_pin_direction( amc_pin, AMC_DIR_OUT );
		amc_gpios_write_pin( amc_pin, 0 );

		// Check: LOW is expected
		pin_val = dut_ctrl_amc_read_pin( amc_pin );
		if( pin_val != 0)
			pin_is_ok = false;

		// Tester goes back to High Z
		amc_gpios_set_pin_direction( amc_pin, AMC_DIR_IN );
	}
	else
		pin_is_ok = false;

	return pin_is_ok;
}



/*
 * Test one pins as input and print the result
 */
static void dut_test_gpio_input_print_single( GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, char* test_label )
{
	// Test
	bool pin_is_ok = dut_test_gpio_input( GPIOx, GPIO_Pin );

	// Print
	mt_printf("%s: ", test_label);
	if( pin_is_ok )
		mt_printf("OK\r\n");
	else
		mt_printf("FAIL\r\n");
}



/*
 * Test a group of GPIO pins as input and print the result
 */
static void dut_test_gpio_input_print_group( GPIO_TypeDef* GPIOx[], uint16_t GPIO_Pin[], int num_of_pins, char* test_label, char* pin_label )
{
	bool general_status_ok = true;
	bool pin_status_ok[35];

	for(int i=0; i<num_of_pins; i++)
		pin_status_ok[i] = true;

	for(int i=0; i<num_of_pins; i++)
	{
		if( dut_test_gpio_input( GPIOx[i], GPIO_Pin[i] ) == false )
		{
			pin_status_ok[i] = false;
			general_status_ok = false;
		}
	}

	// Print
	mt_printf("%s: ", test_label);
	if( general_status_ok )
		mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		for(int i=0; i<num_of_pins; i++)
		{
			if( !pin_status_ok[i] )
				mt_printf("    %s_%d\r\n", pin_label, i );
		}
	}
}


/*
 * Check the AMC Initialization Status and print the result
 */
static uint8_t dut_test_amc_status_print( void )
{
	uint8_t amc_init_status = dut_ctrl_amc_get_status();

	if( amc_init_status != 0 )
		mt_printf("AMC Expanders: INITIALIZATION FAILED\r\n");

	return amc_init_status;
}


/*
 * Test the 12V_EN pin as input and print the result
 *
 * This pin is tested differently since it has a LED pulling its line down.
 */
static void dut_test_12v_en_print( void )
{
	bool pin_is_ok = true;

	TESTER_GPIO_CONFIGURE_PIN( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL );
	HAL_GPIO_WritePin( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_PIN_RESET );
	uint8_t pin_val = dut_ctrl_gpio_read_pin( EN_12V_GPIO_Port, EN_12V_Pin );

	if( !pin_val ) // Due to the LED, pin must be low
	{
		// Tester drives HIGH for checking contact
		TESTER_GPIO_CONFIGURE_PIN( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_MODE_OUTPUT_PP, GPIO_NOPULL );
		HAL_GPIO_WritePin( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_PIN_SET );

		// Check: HIGH is expected
		pin_val = dut_ctrl_gpio_read_pin( EN_12V_GPIO_Port, EN_12V_Pin );
		if( pin_val == 0)
			pin_is_ok = false;

		// Tester goes back to High Z
		TESTER_GPIO_CONFIGURE_PIN( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_MODE_INPUT, GPIO_PULLDOWN );
		HAL_GPIO_WritePin( EN_12V_GPIO_Port, EN_12V_Pin, GPIO_PIN_RESET );
	}
	else
		pin_is_ok = false;

	// Print
	mt_printf("12V_Enable: ");
	if( pin_is_ok )
		mt_printf("OK\r\n");
	else
		mt_printf("FAIL\r\n");
}



/*
 * Test the entire group of AMC pins as input and print the result
 */
static void dut_test_amc_input_print_group( void )
{
	bool general_status_ok = true;
	bool pin_status_ok[90];
	int num_of_pins = 90;

	for(int i=0; i<num_of_pins; i++)
		pin_status_ok[i] = true;

	for(int i=0; i<num_of_pins; i++)
	{
		if( dut_test_amc_input( i ) == false )
		{
			pin_status_ok[i] = false;
			general_status_ok = false;
		}
	}

	// Print
	mt_printf("AMC pins: ");
	if( general_status_ok )
		mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		for(int i=0; i<num_of_pins; i++)
		{
			if( !pin_status_ok[i] )
				mt_printf("    AMC_%d\r\n", i );
		}
	}
}

/*
 * Check the AMC Interrupt Signals and print the result
 */
static void dut_test_amc_interrupt_print( void )
{
	amc_int_status_t interrupt_flags;

	bool global_int_status_ok = true;
	bool pin_int_status_ok[90];

	for(int amc_pin=0; amc_pin<90; amc_pin++)
	{
		dut_ctrl_amc_set_pin_interrupt( amc_pin, AMC_INT_BOTH_EDGES );
		vTaskDelay(pdMS_TO_TICKS(5));

		// Tester drives LOW
		amc_gpios_set_pin_direction( amc_pin, AMC_DIR_OUT );
		amc_gpios_write_pin( amc_pin, 0 );

		// Check Flags
		vTaskDelay(pdMS_TO_TICKS(5));
		interrupt_flags = dut_ctrl_amc_get_int_flag( amc_pin );
		if( interrupt_flags == AMC_INT_STATUS_FALLED )
		{
			pin_int_status_ok[amc_pin] = true;
		}
		else
		{
			global_int_status_ok = false;
			pin_int_status_ok[amc_pin] = false;
		}

		// Tester goes back to High Z; deactivate interrupt
		dut_ctrl_amc_set_pin_interrupt( amc_pin, AMC_INT_OFF );
		amc_gpios_set_pin_direction( amc_pin, AMC_DIR_IN );
	}

	// Print
	mt_printf("AMC interrupts: ");
	if( global_int_status_ok )
	mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		for(int i=0; i<90; i++)
		{
			if( !pin_int_status_ok[i] )
				mt_printf("    AMC_%d\r\n", i );
		}
	}
}


/*
 * Test IPMB-A/B pins as input and print the result
 */
static void dut_test_ipmb_ab_print( uint8_t channel )
{
	bool en_status_ok      = true;
	bool scl_status_ok     = true;
	bool sda_status_ok     = true;

	// Enable channel in both sides
	HAL_GPIO_WritePin      ( ipmb_en_port[channel], ipmb_en_pin[channel], GPIO_PIN_SET );
	dut_ctrl_gpio_write_pin( ipmb_en_port[channel], ipmb_en_pin[channel], GPIO_PIN_SET );

	// Check if it is READY
	if( dut_ctrl_gpio_read_pin( ipmb_rdy_port[channel], ipmb_rdy_pin[channel] ) == 0 )
		en_status_ok = false; // Enabling failed

	if( en_status_ok )
	{
		scl_status_ok = dut_test_gpio_input( ipmb_scl_port[channel], ipmb_scl_pin[channel] );
		sda_status_ok = dut_test_gpio_input( ipmb_sda_port[channel], ipmb_sda_pin[channel] );
	}

	// Disable channel in both sides
	HAL_GPIO_WritePin      ( ipmb_en_port[channel], ipmb_en_pin[channel], GPIO_PIN_RESET );
	dut_ctrl_gpio_write_pin( ipmb_en_port[channel], ipmb_en_pin[channel], GPIO_PIN_RESET );

	// Print
	char channel_letter = 'A' + channel;
	mt_printf("IPMB_%c channel: ", channel_letter);
	if( !en_status_ok )
		mt_printf("ENABLING FAILED\r\n");

	else if(scl_status_ok && sda_status_ok)
		mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		if(scl_status_ok == 0)
			mt_printf("    SCL\r\n" );
		if(sda_status_ok == 0)
			mt_printf("    SDA\r\n" );
	}
}

/*
 * Test I2C pins as input and print the result
 */
static void dut_test_i2c_input_print( GPIO_TypeDef* GPIOx_SCL, uint16_t GPIO_Pin_SCL, GPIO_TypeDef* GPIOx_SDA, uint16_t GPIO_Pin_SDA, char* test_label )
{
	bool scl_status_ok     = true;
	bool sda_status_ok     = true;

	scl_status_ok = dut_test_gpio_input( GPIOx_SCL, GPIO_Pin_SCL );
	sda_status_ok = dut_test_gpio_input( GPIOx_SDA, GPIO_Pin_SDA );

	// Print
	mt_printf("%s : ", test_label);
	if( scl_status_ok && sda_status_ok )
		mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		if(scl_status_ok == 0)
			mt_printf("    SCL\r\n" );
		if(sda_status_ok == 0)
			mt_printf("    SDA\r\n" );
	}
}



/*
 * Test Blue Led as input and print the result
 */
static void dut_test_blue_led_print( void )
{
	bool led_status_ok  = true;

	// Wait for the rising edge
	while( HAL_GPIO_ReadPin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin) == GPIO_PIN_SET )   { asm("nop"); }
	while( HAL_GPIO_ReadPin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin) == GPIO_PIN_RESET ) { asm("nop"); }
	// Check if it is high
	if( dut_ctrl_gpio_read_pin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin) != GPIO_PIN_SET )
		led_status_ok = false;
	// Wait for the falling edge
	while( HAL_GPIO_ReadPin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin) == GPIO_PIN_SET ) { asm("nop"); }
	// Check if it is low
	if( dut_ctrl_gpio_read_pin(FP_LED_BLUE_GPIO_Port, FP_LED_BLUE_Pin) != GPIO_PIN_RESET )
			led_status_ok = false;

	// Print
	mt_printf("Blue Led: ");
	if( led_status_ok )
		mt_printf("OK\r\n");
	else
		mt_printf("FAIL\r\n");

}

/*
 * Test Leds 0, 1 and 2 as output and print the result
 *
 * It checks if the led is blinking. Timeout is used since there is no local reference of blinking.
 */
static void dut_test_leds_print( int led_num )
{
	bool on_status_ok = false;
	bool timeout_flag =  false;
	TickType_t start_time;
	const TickType_t timeout_valule = pdMS_TO_TICKS( 1000 );

	// Wait for the rising edge
	start_time = xTaskGetTickCount();
	while( HAL_GPIO_ReadPin( led_port[led_num], led_pin[led_num] ) == GPIO_PIN_SET )
		if( xTaskGetTickCount() - start_time > timeout_valule )
		{
			timeout_flag = true;
			break;
		}
	start_time = xTaskGetTickCount();
	while( HAL_GPIO_ReadPin( led_port[led_num], led_pin[led_num] ) == GPIO_PIN_RESET )
		if( xTaskGetTickCount() - start_time > timeout_valule )
		{
			timeout_flag = true;
			break;
		}

	TickType_t rise_time = xTaskGetTickCount();

	// Wait for the falling edge
	start_time = xTaskGetTickCount();
	while( HAL_GPIO_ReadPin( led_port[led_num], led_pin[led_num] ) == GPIO_PIN_SET )
		if( xTaskGetTickCount() - start_time > timeout_valule )
		{
			timeout_flag = true;
			break;
		}

	TickType_t fall_time = xTaskGetTickCount();

	TickType_t on_cycle_time = fall_time - rise_time;
	if( (on_cycle_time > pdMS_TO_TICKS(233)) && (on_cycle_time < pdMS_TO_TICKS(433)) )
		on_status_ok = true;


	// Print
	mt_printf("Led %d: ", led_num);
	if( on_status_ok && (!timeout_flag) )
		mt_printf("OK\r\n");
	else
		mt_printf("FAIL\r\n");

}



/*
 * Test UART0 pins as input and print the result
 */
static void dut_test_uart0_input_print( void )
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	bool tx_status_ok     = true;
	bool rx_status_ok     = true;

	// Wait enough time to finish previous test print
	vTaskDelay( pdMS_TO_TICKS(100) );

	TESTER_GPIO_CONFIGURE_PIN( GPIOB, GPIO_PIN_9,  GPIO_MODE_INPUT, GPIO_PULLUP ); // Set TX as GPIO input
	TESTER_GPIO_CONFIGURE_PIN( GPIOH, GPIO_PIN_14, GPIO_MODE_INPUT, GPIO_PULLUP ); // Set RX as GPIO input

	tx_status_ok = dut_test_gpio_input( GPIOB, GPIO_PIN_9 );
	rx_status_ok = dut_test_gpio_input( GPIOH, GPIO_PIN_14 );

	GPIO_InitStruct.Pin = GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	// Print
	mt_printf("UART0 : ");
	if( tx_status_ok && rx_status_ok )
		mt_printf("ALL OK\r\n");
	else
	{
		mt_printf("FAILURE on:\r\n");
		if(tx_status_ok == 0)
			mt_printf("    TX\r\n" );
		if(rx_status_ok == 0)
			mt_printf("    RX\r\n" );
	}
}



/*
 * Test the QSPI bus to the External flash
 */
static void dut_test_qspi_flash_print( void )
{
	bool qspi_flash_status_ok;

	if( dut_ctrl_qspi_get_status() == 0)
		qspi_flash_status_ok = true;
	else
		qspi_flash_status_ok = false;

	// Print
	mt_printf("QSFPI/Flash: ");
	if( qspi_flash_status_ok )
		mt_printf("OK\r\n");
	else
		mt_printf("FAIL\r\n");
}



// UDP receive callback for Network test
static void udp_raw_recv( void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port )
{
	LWIP_UNUSED_ARG(arg);
	if(p == NULL)
		return;

	xQueueSend( udp_recv_queue, &p, 0);
}

/*
 * Network test
 *
 * This test sends some date via UDP and waits for it to be echoed back
 */
static bool dut_test_ntwork(void)
{
	struct udp_pcb *pcb;
	ip_addr_t dut_ipaddr;
	char* test_string = "cachorro";

	bool recv_status = false;
	bool comp_status = false;

	udp_recv_queue = xQueueCreate( 1, sizeof(struct buf*) );

	// Configure UDP socket
	IP_ADDR4(&dut_ipaddr, 192, 168, 0, 11);
	pcb = udp_new();
	udp_bind(pcb, IP_ADDR_ANY, 0);
	udp_recv(pcb , udp_raw_recv, pcb);

	// Send data to DUT
	struct pbuf* p_tx = pbuf_alloc(PBUF_TRANSPORT, strlen(test_string), PBUF_RAM);
	memcpy(p_tx->payload, test_string, strlen(test_string));
	udp_sendto(pcb, p_tx, &dut_ipaddr, 7777);

	// Wait echo from DUT
	struct pbuf* p_rx = NULL;
	if( xQueueReceive(udp_recv_queue, &p_rx, pdMS_TO_TICKS(1000) ) == pdTRUE )
		recv_status = true;

	// Compare Tx and Rx data
	if( recv_status == true )
		if( strcmp( test_string, p_rx->payload ) == 0 )
			comp_status = true;

	// Destroy resources
	if( p_rx != NULL )
		pbuf_free(p_rx);
	pbuf_free(p_tx);
	udp_remove(pcb);
	vQueueDelete( udp_recv_queue );

	return comp_status;

}




