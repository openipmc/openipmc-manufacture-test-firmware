

/*
 * Function codes used by TESTER to control DUT. Both Firmware projects
 * share this definitions
 */
#define CMD_GPIO_SET_PIN_AS_OUT 0x01
#define CMD_GPIO_SET_PIN_AS_IN  0x02
#define CMD_GPIO_SET_PIN_HIGH   0x03
#define CMD_GPIO_SET_PIN_LOW    0x04
#define CMD_GPIO_READ_PIN       0x05
#define CMD_GET_MCU_UID         0x06
#define CMD_AMC_SET_PIN_AS_OUT  0x07
#define CMD_AMC_SET_PIN_AS_IN   0x08
#define CMD_AMC_SET_PIN_HIGH    0x09
#define CMD_AMC_SET_PIN_LOW     0x0A
#define CMD_AMC_SET_PIN_INT     0x0B
#define CMD_AMC_READ_PIN        0x0C
#define CMD_AMC_GET_STATUS      0x0D
#define CMD_AMC_GET_INT_FLAGS   0x0E
#define CMD_GET_VERSION         0x0F
#define CMD_SET_SWD_TO_GPIO     0x10
#define CMD_QSPI_GET_STATUS     0x11
